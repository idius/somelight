import ephem
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as tri


itwo_pi = 1 / (2 * np.pi)


def irradiance(alt):
    """
    Computes the irradiance (power per unit area) received at the Earth's surface, given the
    altitude angle of the Sun. In reality, this quantity varies with the time of year, latitude,
    altitude above sea level, humidity, atmospheric dust content, and weather. This estimate should
    be accurate to within ~20% for most of Earth's surface throughput the year, assuming clear skies
    (i.e., cloudless sky with no smoke or volcanic ash present). The error is more than 20% when
    the Sun is near the horizon (morning and evening, when alt < 10 degrees). In such cases, the
    irradiance is under-estimated by more than 20% when the atmosphere is clear.
    :param alt: the altitude angle of the Sun, in radians
    :return: The estimated irradiance in Watt / meter^2
    """
    if alt <= 0:
        return 0
    zenith = np.pi / 2 - alt
    cz = np.cos(zenith)
    airmass = 1.0 / (cz + 0.025 * np.exp(-11 * cz))
    return 1230 * np.exp(-0.15 * airmass)


def is_inside(point, polygon):
    """
    returns True iff the point (x, y) is within the polygon [(x1, y1), (x2, y2), (x3, y3), ...]
    This is an implementation of the winding number algorithm which solves the point-in-polygon problem.
    """
    poly = np.array(polygon)

    p = np.array(point)

    vectors = poly - p

    vector_magnitudes = np.hypot(vectors.T[0], vectors.T[1])

    # make sure that the point isn't the same as a vertex

    if np.sum(vector_magnitudes < 1e-15) > 0:
        return True

    mags = np.array([vector_magnitudes, vector_magnitudes]).T

    normed_vectors = vectors / mags

    normed_vectors_permuted = np.roll(normed_vectors, -1, axis=0)

    cross_prod = (normed_vectors.T[0] * normed_vectors_permuted.T[1]
                  - normed_vectors.T[1] * normed_vectors_permuted.T[0])

    sign = np.sign(cross_prod)

    dot_prod = np.sum(normed_vectors * normed_vectors_permuted, axis=1)

    # avoid situations in which the normalization is slightly too large,
    # due to rounding (i.e., when the dot product is > 1.0)

    mask = (np.abs(dot_prod) <= 1.0)

    dot_prod = dot_prod * mask + (1.0 - mask)

    dtheta = np.arccos(dot_prod)

    dt = sign * dtheta

    winding_number = dt.sum() * itwo_pi

    return np.abs(winding_number) > 1e-6


class PlaneSegment(object):
    """
    The PlaneSegment class represents a single plane surface. Objects of this type are used to compute
    the coordinate transformations and projections needed in order to determine whether a particular
    point is in direct sunlight.
    """

    def __init__(self, vertices, observer, sun, north):
        self.vertices = vertices
        self.vertices.append(vertices[0])  # complete the loop / close the polygon
        self.observer = observer
        self.sun = sun
        self.north = north

    def is_shaded(self, pos, sun_coords=None):
        """
        returns True if the position x,y,z is in the shadow of this plane
        """
        projected_plane = self.project_onto_alt_az(pos)

        if sun_coords is None:
            sun_position = self.sun_position(self.observer)
        else:
            sun_position = sun_coords

        for poly in projected_plane:
            if is_inside(sun_position, poly):
                return True

        return False

    def project_onto_alt_az(self, pos):
        """
        Project the plane from Cartesian coordinates onto Altitude-Azimuth coordinates (az, alt)
        If the plane is partially behind pos (i.e., if any y-coordinate of the vertices is less than the y-component of
        the observation position, pos, then break the shape into two segments: a front half and a back half)
        """

        here = np.array(pos)

        verts = np.array(self.vertices)

        behind = (verts.T[1] - here[1]) < 0.0

        n_vertices_behind = behind[:-1].sum()

        alt_az_shapes = []

        if 0 < n_vertices_behind < len(self.vertices[:-1]):
            polygon1, polygon2 = self.split_poly(self.vertices, here)
            poly1_is_rear = np.array(polygon1).T[1].mean() < here[1]
            poly2_is_rear = np.array(polygon2).T[1].mean() < here[1]
            # TODO try changing things so that the trace_poly step is performed only once, in the constructor.
            # then compare performance with the current implementation. This requires the trace_poly output to be
            # Cartesian coordinates (currently, the projection onto horizontal coordinates is done inside of trace_poly).
            poly1_alt_az = self.trace_poly(polygon1, here, poly1_is_rear)
            poly2_alt_az = self.trace_poly(polygon2, here, poly2_is_rear)
            alt_az_shapes = [poly1_alt_az, poly2_alt_az]
        else:
            is_rear = np.array(self.vertices).T[1].max() < here[1]
            alt_az_shapes.append(self.trace_poly(self.vertices, here, is_rear))

        return alt_az_shapes

    def split_poly(self, vertices, here):
        """
        splits a polygon, defined by a set vertices, into two smaller polygons---one in front of the point, here,
        and the other behind the point, here.
        :param vertices:
        :param here:
        :return:
        """
        disp = np.array(vertices) - here

        # identify which vertices lie immediately on either side of a cut

        cut_vertices = []

        for i in range(1, len(disp)):
            if disp[i][1] * disp[i - 1][1] < 0:
                # the sign changed; the line connecting these points crosses the y = here[1] plane
                cut_vertices.append(i - 1)
                cut_vertices.append(i)

        # Now, using the cut_vertices, identify the cut points (the points of intersection) and insert these points to
        # create two new polygons. The first two vertices in the cut_vertices list define one line, the second two
        # define the other line. There should be no more than 2 lines that intersect the y = here[1] plane.

        neighbor00 = np.array(vertices[cut_vertices[0]])
        neighbor01 = np.array(vertices[cut_vertices[1]])

        dn0 = neighbor01 - neighbor00

        t0 = (here[1] - neighbor00[1]) / (neighbor01[1] - neighbor00[1])  # parameter t = y0 / (y0 - y1)

        cut0 = neighbor00 + dn0 * t0  # coordinates of the first cut point

        cut0 = tuple(cut0.tolist())

        neighbor10 = np.array(vertices[cut_vertices[2]])
        neighbor11 = np.array(vertices[cut_vertices[3]])

        dn1 = neighbor11 - neighbor10

        t1 = (here[1] - neighbor10[1]) / (neighbor11[1] - neighbor10[1])

        cut1 = neighbor10 + dn1 * t1

        cut1 = tuple(cut1.tolist())

        # The final entry in the vertices list is the same as the first entry, in order to close the polygon.
        # before continuing, remove the redundant final entry:

        all_verts = vertices[:-1]

        # insert the new vertices

        insert_index0 = cut_vertices[1]
        insert_index1 = cut_vertices[3]

        all_verts.insert(insert_index1, cut1)
        all_verts.insert(insert_index1, ';')
        all_verts.insert(insert_index1, cut1)

        all_verts.insert(insert_index0, cut0)
        all_verts.insert(insert_index0, ';')
        all_verts.insert(insert_index0, cut0)

        poly0 = []
        poly1 = []

        selector_on = False  # this is used as a toggle switch

        for v in all_verts:
            if v == ';':
                selector_on = not selector_on
                continue
            if selector_on:
                poly0.append(v)
            else:
                poly1.append(v)

        # add redundant final vertex

        poly0.append(poly0[0])
        poly1.append(poly1[0])

        return poly0, poly1

    def trace_poly(self, vertices, here, rear=False):
        """
        Traces around the edges of a polygon defined in Cartesian coordinates, so that the edges can
        be properly projected and transformed into alt-az coordinates.
        :param vertices:
        :param here:
        :param rear:
        :return:
        """

        first_vertex = np.array(vertices[0])

        alt_az_vertices = [self.cartesian_to_alt_az(first_vertex, here, rear)]

        for idx in range(len(vertices) - 1):
            vert = np.array(vertices[idx])
            vert_next = np.array(vertices[idx + 1])
            dr = (vert_next - vert) / 25.0
            for s in np.arange(1, 26):
                v = vert + s * dr
                alt_az_vertices.append(self.cartesian_to_alt_az(v, here, rear))

        return alt_az_vertices

    def cartesian_to_alt_az(self, coords, pos, rear=False):
        """
        Maps the Cartesian coordinates (z, y, z) to altitude-azimuth coordinates (azimuth, altitude),
        given the observation position, pos (z, y, z).
        """
        r = np.array(coords) - np.array(pos)
        floor_distance = np.hypot(r[0], r[1])
        alt = np.arctan2(r[2], floor_distance)
        azimuth = np.arctan2(r[1], r[0])

        if rear and azimuth > 0:
            azimuth *= -1.0

        # shift the angle into [0, 2pi]
        if rear:
            azimuth += 2 * np.pi

        if (not rear) and azimuth < 0:
            azimuth *= -1.0

        # change from ordinary spherical sign convention to the convention used in the horizontal (alt-az)
        # coordinate system
        azimuth = 2 * np.pi - azimuth

        return azimuth, alt

    def sun_position(self, observer):
        self.sun.compute(observer)
        az = float(self.sun.az + self.north * np.pi / 180.0) % (2 * np.pi)
        return az, float(self.sun.alt)


class GardenVolume(object):
    """
    The GardenVolume class is used to model a garden site. Once the user enters a description
    of the garden, methods of the class can be used to compute various useful quantities,
    related to sunlight exposure at any location within the volume.
    """
    def __init__(self, latitude, longitude, north, window_mode=False):

        self.longitude = longitude
        self.latitude = latitude
        self.north = north
        self.window_mode = window_mode
        self.planes = []
        self.observer = ephem.Observer()
        self.observer.lat = str(latitude)
        self.observer.lon = str(longitude)
        self.sun = ephem.Sun()
        self.time_zone_offset = ephem.now() - ephem.Date(ephem.localtime(ephem.now()))

    def add_plane_segment(self, vertices):
        """
        Creates a plane object from the (x, y, z) coordinates of the 4 corners of the plane
        The plane segments that I have in mind represent sections of wall and ceiling / roof.
        TODO: sort by the likelihood that this plane will cast a shadow on a point in the garden
        (as a first approximation, sort by area. Other criteria may lead to better results)
        """

        plane = PlaneSegment(vertices, self.observer, self.sun, self.north)

        self.planes.append(plane)

    def sun_position(self):
        self.sun.compute(self.observer)
        az = float(self.sun.az + self.north * np.pi / 180.0) % (2 * np.pi)
        return az, float(self.sun.alt)

    def irradiance(self, points, time):
        """
        Computes the approximate irradiance [Watt / meter^2]
        """
        self.observer.date = ephem.Date(ephem.Date(time) + self.time_zone_offset)

        self.sun.compute(self.observer)

        alt = self.sun.alt

        return np.array(self.illuminated_points(points, time)) * irradiance(alt)

    def illuminated_points(self, points, time):
        """
        Tells us whether the x,y,z position (pos) is directly-lit by the Sun at a given time (date).
        Returns True if pos is directly-lit, False otherwise
        """
        self.observer.date = ephem.Date(ephem.Date(time) + self.time_zone_offset)

        self.sun.compute(self.observer)

        results = []

        # if the sun is below the horizon, then don't bother with the more complicated procedures

        if float(self.sun.alt) < 0:
            for pos in points:
                results.append(False)
            return results

        # if the point is shaded by any of the planes, then it is not in direct sunlight

        if not self.window_mode:
            for pos in points:
                result = True
                for plane in self.planes:
                    if plane.is_shaded(pos):
                        result = False
                        break
                results.append(result)
        else:  # if the point is "shaded" by a window, then it is in direct sunlight
            for pos in points:
                result = False
                for plane in self.planes:
                    if plane.is_shaded(pos):
                        result = True
                        break
                results.append(result)

        return results

    def exposure_on_date(self, points, date, resolution=5):
        """
        Computes the total number of minutes of sun exposure that a set of points receive on a specific date.
        :param points: a list of (x, y, z) coordinates for which to measure the direct sun exposure
        :param date: the date of interest, in Year/Month/day format (for example: "2017/12/21")
        :param resolution: The time resolution (time step size) for the integration.
        :return: A numpy array of exposure times, expressed in minutes.
        """
        date = ephem.Date(date)

        # the temporal resolution in minutes
        dt_minutes = resolution

        # the temporal resolution in days
        dt_days = dt_minutes / (60.0 * 24.0)

        n_samples_per_day = int(1.0 / dt_days)

        total_time = np.zeros(len(points))

        for t in range(n_samples_per_day):
            print(24 * t / float(n_samples_per_day))
            mask = np.array(self.illuminated_points(points, date + t * dt_days))
            total_time += mask * dt_minutes

        return total_time

    def radiant_exposure_on_date(self, points, date, resolution=5):

        date = ephem.Date(date)

        # the temporal resolution in minutes
        dt_minutes = resolution

        # the temporal resolution in days
        dt_days = dt_minutes / (60.0 * 24.0)

        n_samples_per_day = int(1.0 / dt_days)

        total_energy = np.zeros(len(points))

        # The total radiant exposure during one day is the time integral of the irradiance.
        # TODO: approximate the integral using the trapezoid rule and increase the time step size)
        for t in range(n_samples_per_day):
            print(24 * t / float(n_samples_per_day))
            mask = np.array(self.irradiance(points, date + t * dt_days))
            total_energy += mask * 60 * dt_minutes

        return total_energy * 1e-6  # convert from J / m^2 to MJ / m^2

    def mean_exposure_during_period(self, points, start_date, number_of_days):

        times = self.daily_exposure_during_period(points, start_date, number_of_days)

        return np.mean(times, axis=0)

    def mean_radiant_exposure_during_period(self, points, start_date, number_of_days):

        times = self.daily_radiant_exposure_during_period(points, start_date, number_of_days)

        return np.mean(times, axis=0)

    def mean_daily_exposure(self, points):
        """
        :param points: a list of (x, y, z) coordinates for which to measure the direct sun exposure
        :return: A list containing the mean number of minutes of direct sun exposure each point in the points
        list experiences each day in the year 2017
        """
        return self.mean_exposure_during_period(points, '2017/1/1', 365)

    def mean_daily_radiant_exposure(self, points):
        """
        :param points: a list of (x, y, z) coordinates for which to measure the direct sun exposure
        :return: A list containing the mean radiant exposure that each point in the points
        list experiences each day in the year 2017
        """
        return self.mean_radiant_exposure_during_period(points, '2017/1/1', 365)

    def daily_exposure_during_period(self, points, start_date, number_of_days):
        """
        Computes the number of minutes of direct sunlight exposure for each day in a specified time range
        :param points: a list of (x, y, z) coordinates for which to measure the direct sun exposure
        :param start_date: the first day of interest
        :param number_of_days: The number of days in the time period of interest
        :return: returns a NumPy array, containing, for each input point, 365 entries corresponding to
        the number of minutes of direct sun exposure that the point experiences.
        """
        start_date = ephem.Date(start_date)

        all_times = []

        for day in range(number_of_days):
            times = self.exposure_on_date(points, start_date + day)
            all_times.append(times)

        return np.array(all_times)

    def daily_radiant_exposure_during_period(self, points, start_date, number_of_days):
        """
        Computes the radiant exposure for each day in a specified time range
        :param points: a list of (x, y, z) coordinates for which to measure the direct sun exposure
        :param start_date: the first day of interest
        :param number_of_days: The number of days in the time period of interest
        :return: returns a NumPy array, containing, for each input point, 365 entries corresponding to
        the number of minutes of direct sun exposure that the point experiences.
        """
        start_date = ephem.Date(start_date)

        all_times = []

        for day in range(number_of_days):
            times = self.radiant_exposure_on_date(points, start_date + day)
            all_times.append(times)

        return np.array(all_times)

    def is_illuminated(self, pos, time, show_plot=False):
        """
        Tells us whether the x,y,z position (pos) is directly-lit by the Sun at a given time (date).
        Returns True if pos is directly-lit, False otherwise
        """
        self.observer.date = ephem.Date(ephem.Date(time) + self.time_zone_offset)

        self.sun.compute(self.observer)

        # if the sun is below the horizon, don't bother with the more complicated procedures

        if float(self.sun.alt) < 0:
            return False

        if show_plot:
            self.show_projected_region(pos, self.planes)

        if not self.window_mode:  # if the point is shaded by any of the planes, then it is not in direct sunlight.
            for plane in self.planes:
                if plane.is_shaded(pos):
                    return False
            return True
        else:  # if the point is "shaded" by a window, then it is in direct sunlight.
            for plane in self.planes:
                if plane.is_shaded(pos):
                    return True
            return False

    def is_illuminated_alt_az(self, pos, sun_coordinates):

        # if the sun is below the horizon, don't bother with the more complicated procedures

        if sun_coordinates[1] <= 0:
            return 0.0

        if not self.window_mode:  # if the point is shaded by any of the planes, then it is not in direct sunlight.
            for plane in self.planes:
                if plane.is_shaded(pos, sun_coordinates):
                    return False
            return True
        else:  # if the point is "shaded" by a window, then it is in direct sunlight.
            for plane in self.planes:
                if plane.is_shaded(pos, sun_coordinates):
                    return True
            return False

    def radiant_exposure_time_series(self, point, start_date, duration, timestep_minutes):

        positions = self.sun_positions(start_date, duration, timestep_minutes)

        return [self.is_illuminated_alt_az(point, p) * irradiance(p[1]) for p in positions]

    def sun_positions(self, start_date, duration, timestep_minutes):

        start = ephem.Date(start_date)

        end = ephem.Date(start_date) + duration

        step_size_days = timestep_minutes / 1440.0

        n_steps = int((end - start) / step_size_days)

        self.observer.date = ephem.Date(start + self.time_zone_offset)

        positions = []

        for t in range(n_steps):
            self.observer.date = ephem.Date(self.observer.date + step_size_days)
            positions.append(self.sun_position())

        return positions

    def show_projected_region(self, pos, planes):

        for plane in planes:
            self.plot_projected_polygon(pos, plane)

        sun_alt = self.sun.alt * 180 / np.pi
        sun_az = self.sun.az * 180 / np.pi

        plt.scatter([(sun_az + self.north) % 360.0], [sun_alt], marker='*')

        plt.xlim([-10, 370])
        plt.ylim([-10, 100])
        plt.axis('tight')
        plt.axes().set_aspect('equal')
        plt.show()
        plt.close()

    def show_garden(self, points, date):

        date = ephem.Date(date)

        results = self.illuminated_points(points, date)
        pts = np.array(points)

        mask = np.array(results)
        xs = pts[mask].T[0]
        ys = pts[mask].T[1]
        plt.scatter(xs, ys)
        plt.axes().set_aspect('equal')
        plt.show()

    @staticmethod
    def plot_projected_polygon(pos, polygon):

        all_polygons = polygon.project_onto_alt_az(pos)

        for verts in all_polygons:
            verts = np.array(verts) * 180 / np.pi
            plt.plot(verts.T[0], verts.T[1])

    @staticmethod
    def show_interpolation(points, values, max=None, name=None, title=None):
        x = np.array(points).T[0]
        y = np.array(points).T[1]
        triang = tri.Triangulation(x, y)
        if max is None:
            max = np.array(values).max()

        plt.tricontourf(x, y, values, 40, cmap=plt.cm.rainbow,
                        norm=plt.Normalize(vmin=0.0, vmax=max))
        plt.colorbar()

        if title is not None:
            plt.title(title)

        if name is None:
            plt.show()
            plt.close()
        else:
            plt.savefig(name)
            plt.close()

# try using this https://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection
# For methods that use more than one time/date, make a list of (az, alt, nx, ny, nz) where n* are the components of
# a direction vector pointing toward the sun. Perform a loop over this array for each point of interest in order to
# avoid re-computing the geometry for each timestep