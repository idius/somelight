import somelight
import numpy as np
from joblib import Parallel, delayed
import ephem


def setup():
    # The latitude and longitude of the garden
    latitude = 33.9853
    longitude = -117.5527

    # The direction of the +x axis of the local Cartesian coordinate system, measured
    # in degrees. North is defined as 0.0 and the angle increases in a counter-clockwise
    # sense (westward from North), such that West is 90 degrees and East is 270 degrees.
    right = 103

    # create a GardenVolume object

    garden = somelight.GardenVolume(latitude, longitude, right)

    # Specify the primary surfaces that can shade the garden. These should approximately
    # be plane segments, however mild curvature is generally okay (the points do not need
    # to be absolutely coplanar). Although the examples below are rectangles, any shape with
    # at least three distinct vertices will work. Note that any length unit can be used, since
    # only the RELATIVE positions and sizes of the planes are relevant.

    # East wall
    garden.add_plane_segment([(0, 0.0, 0), (0, 6, 0), (0, 6, 8), (0, 0.0, 8)])

    # North wall
    garden.add_plane_segment([(0, 0.0, 0), (6.0, 0, 0), (6, 0, 8), (0, 0.0, 8)])

    # roof
    garden.add_plane_segment([(-2.0, 6.5, 8), (6.5, 6.5, 8), (6.5, 0.0, 8), (-2.0, 0.0, 8)])

    # North side of column
    garden.add_plane_segment([(4.6, 4.6, 0), (6.0, 4.6, 0), (6.0, 4.6, 8), (4.6, 4.6, 8)])

    # East side of column
    garden.add_plane_segment([(4.6, 4.6, 0), (4.6, 6.0, 0), (4.6, 6.0, 8), (4.6, 4.6, 8)])

    # Define the points of interest. These can be distributed throughout the volume of
    # interest in any way you wish. In some cases, it may be more efficient to randomly
    # sample points. In this example, the sampling is done uniformly in a planar region
    # slightly above the floor of the garden.

    points = []

    for x in np.arange(0.1, 4.5, 0.15):
        for y in np.arange(0.1, 6.0, 0.15):
            points.append((x, y, 0.1))

    for x in np.arange(4.5, 6.0, 0.15):
        for y in np.arange(0.1, 4.5, 0.15):
            points.append((x, y, 0.1))

    return garden, points


def example1():
    """
    Displays the points that receive direct sun exposure at a specific moment.
    """

    garden, points = setup()

    # Draw a plot showing which points are illumnated at a specific moment.

    garden.show_garden(points, '2016/12/30 12:32:30')


def example2():
    """
    Computes the direct exposure time for a set of points on a single day and
    displays an exposure map.
    """
    garden, points = setup()

    # Compute the total exposure time at each sample point during a specific day.

    exposure_times = garden.exposure_on_date(points, '2016/12/30')

    # when the sample points are distributed in a plane, you can create a meaningful
    # 2-D map of the direct sun exposure time.

    garden.show_interpolation(points, exposure_times)


def example3():
    """
    Computes the radiant exposure for a set of points on a single day and
    displays a radiant exposure map.
    """
    garden, points = setup()

    # Compute the radiant exposure at each sample point during a specific day.

    rad_exp = garden.radiant_exposure_on_date(points, '2016/12/30')

    # when the sample points are distributed in a plane, you can create a meaningful
    # 2-D map of the radiant exposure.

    garden.show_interpolation(points, rad_exp)


def rad_exp_single_thread(day):
    """
    This fenction is used to facilitate parallelization in example 4
    """
    garden, points = setup()
    exposure_times = garden.radiant_exposure_on_date(points, ephem.Date('2017/1/1') + day)
    return exposure_times


def example4():
    """
    Computes the radiant exposure for a set of points for every Sunday of the year 2017 and saves
    the images as files.
    """

    # compute the radiant exposure for every day of the year, using joblib to distribute the work
    # over 7 simultaneous threads:

    exposures = Parallel(n_jobs=7)(delayed(rad_exp_single_thread)(i) for i in range(0, 365, 7))

    garden, points = setup()

    # draw one plot plot for every week of the year

    for day, ex in enumerate(exposures):
        if len(ex) != len(points):
            print("len ex", len(ex))
            print("len points", len(points))
            return 0
        garden.show_interpolation(points,
                                  ex,
                                  max=32,
                                  name="balcony_rad_exp_{:03d}.png".format(7*day),
                                  title=str(ephem.Date(ephem.Date('2017/1/1') + 7*day))[:-9])

